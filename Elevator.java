/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package elevatorass1;

import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;


/**
 *
 * @author Nicole
 */
public class Elevator {
    int delay = 4; 
    int capacity;
    int maxCapacity = 10;
    int floorNum;
    final int defaultFloor = 1;
    boolean isMoving;
    List<Integer> floorOrder = new ArrayList<>();  // change
    Door elevatorDoor = new Door() ;
    Queue elevatorQueue = new Queue() ;
    
   
    
    
    public void moveToDesiredFloor() {

    elevatorDoor.close();
    
    isMoving = true;
   
    // code to make elevator move to floor
   
    stop();
    elevatorDoor.open();
    }
    
    public void stop() {
    isMoving = false;
    
    } 
    
    public void returnToDefaultFloor() {
        isMoving = true;
        floorNum = defaultFloor;
        stop();
    
    }
}